import type { NextPage } from "next";
import Head from "next/head";
import styles from "../styles/Home.module.css";

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>
          Georgi Alexandrov&quot;s final project for Telerik DevOps course
        </title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Overengineered hello world2</h1>

        <p className={styles.description}>
          Final project for{" "}
          <a
            target="_blank"
            rel="noreferrer"
            href="https://www.telerikacademy.com/upskill/devops"
          >
            Telerik Academy Upskill Devops course!
          </a>{" "}
          by{" "}
          <a
            target="_blank"
            rel="noreferrer"
            href="https://georgi.alexandrov.dev"
          >
            Georgi Alexandrov
          </a>
        </p>
        <p>
          <a
            target="_blank"
            rel="noreferrer"
            href="https://gitlab.com/georgialexandrov/devops-telerik-course-final-project"
          >
            See GitLab repo
          </a>
        </p>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by <span className={styles.logo}>Vercel</span>
        </a>
      </footer>
    </div>
  );
};

export default Home;
