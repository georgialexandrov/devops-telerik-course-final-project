# Overengineered Hello World Application

Final project for Telerik DevOps course

## Solution

The solution includes the following topics from the course conspect:

- Source control (this repo)

- GitLab is chosen because it is used in my company and the research for this project helps me to further develop our pipelines

- Building pipeline (see below)

- Continuous Integration

  1. New build on every commit to `main` branch. The build publishes the javascript code as artifact.
  2. SonarCloud Scan and Unit Tests run in parallel.
  3. Project build is generating `out` directory with static html.
  4. If the build is successful, this step generates `CHANGELOG` and sets new semantic version to `package.json` and pushes a tag with this version.

```mermaid
graph LR;
    A["Push to repo"]-->B["☑ SonarCloud check"];
    A-->C["☑ Run tests"];
    B-->D["🔨 Build project"];
    C-->D;
    D-->E["🆕 Generate new version"]
```

- Continuous Delivery

  1. Once a new tag is pushed, docker image is created and pushed to GitLab container repository.
  2. Terraform is updating Azure image and sets the IP in the DNS of domain.
  3. Playwright test checks if the deployed website is working correctly in 3 main browsers.
  4. Announces the new version in Signal.

```mermaid
graph LR;
    A["New tag pushed"]-->B["🔨 Build Docker image"];
    B-->C["☁️ Deploy and update DNS"];
    C-->D["✔ Sanity Check"];
    D-->E["📬 Notify in Signal"]
```

- Security (SAST scan through Sonar Cloud)

- Docker (the project is build as a Docker image pushed to GitLab container registry)

- Public Cloud (the solution is pushed to Microsoft Azure Container Instance)

- Send notification to Signal through GitLab Runner when the build is done
  - GitLab runner on local server for the test. Signal does not have webhook support and exposing the API brihgs security risk. GitLab Runner works as reverse shell and does not require any open ports.
- Flavoured markdown (GitLab)
  - Versioning for the documentation (including charts)

# Technologies used

- [Next.js](https://nextjs.org) for the Hello World
- [Jest](https://jestjs.io) for unit testing
- [Standard version](https://github.com/conventional-changelog/standard-version#readme) npm module for generating semantic versioning out of [Conventional Commits](http://conventionalcommits.org)
- [Terraform](https://www.terraform.io) for infrastructure as a code
- [Microsoft Azure](http://azure.microsoft.com) as public cloud
- [SonarCloud](https://sonarcloud.io) for Static Application Security Testing
- [Playwright](http://playwright.dev) for end to end testing
- [Flavoured Markdown](https://docs.gitlab.com/ee/user/markdown.html) for better readme file
- [Mermaid](https://mermaid-js.github.io/mermaid/#/) for the magnificent diagrams
- [Signal](https://signal.org) to receive notificatio while keeping privacy.
- [GitLab Runner](https://docs.gitlab.com/runner/) to send the signal message in a secure environment (server at home)
- [Cloudflare](http://cloudflare.com) for managing DNS

# Future improvements

- DAST
- Shifting left
  - Run testunit tests and SonarCloud on push in the branch
  - Build on merge to master
- Terraform cloud for easier handling of the state
