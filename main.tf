terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "overcomplicated_hello_world" {
  name                = "overcomplicated_hello_world"
  location            = "eastus"
}

resource "azurerm_container_group" "overcomplicated_hello_world" {
  name                = "overcomplicated_hello_world"
  location            = azurerm_resource_group.overcomplicated_hello_world.location
  resource_group_name = azurerm_resource_group.overcomplicated_hello_world.name
  ip_address_type     = "public"
  os_type             = "linux"

  image_registry_credential {
    server   = "registry.gitlab.com"
    username = "${var.gitlab_username}"
    password = "${var.gitlab_password}"
  }

  container {
    name   = "hw"
    image  = "registry.gitlab.com/georgialexandrov/devops-telerik-course-final-project:${var.docker_image_tag}"
    cpu    = "0.5"
    memory = "0.5"

    ports {
      port     = 80
      protocol = "TCP"
    }
  }

  tags = {
    environment = "testing"
  }

  lifecycle {
    create_before_destroy = true
  }
}

provider "cloudflare" {}

resource "cloudflare_record" "owendavies-net" {
  zone_id = "526a337db74782b5f2d02407392e90ff"
  name = "overengineered-hello-world"
  value = azurerm_container_group.overcomplicated_hello_world.ip_address
  type = "A"
  proxied = true
  ttl = 1
}

variable "gitlab_username" {
  type = string
}

variable "gitlab_password" {
  type = string
}

variable "docker_image_tag" {
  type = string
}
