import { test, expect } from "@playwright/test";

test("Sanity check", async ({ page }) => {
  // await page.goto("http://localhost:3000");
  await page.goto("https://overengineered-hello-world.alexandrov.dev/");

  const title = page.locator("h1");
  await expect(title).toHaveText("Overengineered hello world2");
});
