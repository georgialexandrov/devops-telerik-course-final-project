# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.9.9](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.9.8...v1.9.9) (2022-01-25)


### Bug Fixes

* **devops:** declare terraform variable for docker tag ([47943e5](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/47943e56e762226e48a3ede257cdc24a4a5f9b80))

### [1.9.8](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.9.7...v1.9.8) (2022-01-25)


### Bug Fixes

* **pages:** another test ([7d4c914](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/7d4c914c1b1463084c317615226bc6f49921c89d))

### [1.9.7](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.9.6...v1.9.7) (2022-01-25)


### Bug Fixes

* **deploy:** add tag to docker image ([d2c7c36](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/d2c7c3634b5da93208b1816f20b9eabfb9a3d412))

### [1.9.6](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.9.5...v1.9.6) (2022-01-25)


### Bug Fixes

* **pages:** text change ([7c3ac95](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/7c3ac95be1194aa179d63ed9c3802aa3a219dda2))

### [1.9.5](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.9.4...v1.9.5) (2022-01-25)


### Bug Fixes

* **devops:** change text ([2b85cff](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/2b85cff36824d3e8529c44b4fd03439c294e6ca7))

### [1.9.4](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.9.3...v1.9.4) (2022-01-25)

### [1.9.3](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.9.2...v1.9.3) (2022-01-25)


### Bug Fixes

* **devops:** heavy testing 1 ([0356ac7](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/0356ac7f8930a66651a6fcf174f8052ba0f44204))

### [1.9.2](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.9.1...v1.9.2) (2022-01-25)


### Bug Fixes

* **devops:** heavy testing ([3a94107](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/3a9410771b0a3bd7e5aa7c395a248fbcc37251d4))
* **devops:** test change + deploy + sanity check ([32ea5e1](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/32ea5e10ebaf496b16f55fc3de59466aff3c712d))

### [1.9.1](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.9.0...v1.9.1) (2022-01-25)


### Bug Fixes

* **pages:** one more build ([4c89d81](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/4c89d81e09a088abf827d4d7ca8651628a4e8051))
* **page:** test deploy ([41975ee](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/41975ee9e87f05ba7fbaeacfefdc07a97099dfaf))

## [1.9.0](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.6.0...v1.9.0) (2022-01-25)


### Features

* **devops:** build on page change, deploy on tag push ([e369d12](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/e369d1231ce1fa8a6eff8abcc4d03ddd70f902e8))
* **devops:** notify in Signal about the new build ([85b9b14](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/85b9b14b1415557bd1fd4afe04c035179d30763b))
* **devops:** run sanity check after deploy ([dd1fe84](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/dd1fe84998fa4dce34aa8d08c3a1f461fd10c147))

## [1.8.0](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.6.0...v1.8.0) (2022-01-25)


### Features

* **devops:** notify in Signal about the new build ([85b9b14](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/85b9b14b1415557bd1fd4afe04c035179d30763b))
* **devops:** run sanity check after deploy ([dd1fe84](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/dd1fe84998fa4dce34aa8d08c3a1f461fd10c147))

## [1.7.0](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.6.0...v1.7.0) (2022-01-25)


### Features

* **devops:** notify in Signal about the new build ([85b9b14](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/85b9b14b1415557bd1fd4afe04c035179d30763b))

## [1.6.0](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.5.5...v1.6.0) (2022-01-24)


### Features

* **devops:** update subdomain ip ([a9529ae](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/a9529ae0a74d367efd06eb6434f0ec66580e08ef))

### [1.5.5](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.5.4...v1.5.5) (2022-01-23)

### [1.5.4](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.5.0...v1.5.4) (2022-01-23)


### Bug Fixes

* **devops:** allow rebuild ([a3a66ce](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/a3a66ce331871b7d1c15d8a19de1132a09251a92))

## [1.5.0](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.4.1...v1.5.0) (2022-01-23)


### Features

* **devops:** Add Terraform deploy ([2c774b5](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/2c774b56e569d3da840ba34dc44632b789c258ab))


### Bug Fixes

* **devops:** implement T-shaped solution ([a8fa16d](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/a8fa16d40e722c4053b8c3e2fcd357e3396a4723))

### [1.4.1](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/compare/v1.4.0...v1.4.1) (2022-01-16)


### Bug Fixes

* **devops:** dummy change to test the build ([043d4d7](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/043d4d7e9d2a6ad19b771a35896ea5efecaf6f9e))
* **devops:** run ci job only when something really changes ([5fa15c5](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/5fa15c5d5d65eb3d4bdc20adc5bd1638feb5a36b))
* **pages:** another dummy commit to test build ([a10ca52](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/a10ca52efe935b7caf58bde96c01a7569b7db13a))
* **pages:** another dummy commit to test build ([045b23c](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/045b23c19d760ed73a475d81b30629a6ea57ec3c))

## 1.4.0 (2022-01-16)


### Features

* **devops:** build docker image after merge to master ([c1815e4](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/c1815e43816a3f5cfef4330fc261343f555913c4)), closes [#5](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/5)
* **devops:** build next app on push ([12b7dbe](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/12b7dbebf75ff09fad9d6df46029cf8c7e942573)), closes [#3](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/3)
* **devops:** generate version and changelog on build ([5e2fa69](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/5e2fa6940612906662a83c3cdd3cecd4d41c57fc)), closes [#4](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/4)
* **devops:** Implement sonar cloud ([62cbd87](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/62cbd877d9ef8560afe7daa84cf0ea7f86a42c4c)), closes [#1](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/1)
* **devops:** run unit tests on build ([1929d0b](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/1929d0bd5b2e9b6d47c448b13a2e25cc085ff15c)), closes [#2](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/2)
* **homepage:** write down some more information what is this project about ([76fa419](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/76fa419d15b91cbe2bd98b99847010d257cff343))


### Bug Fixes

* **pages:** remove unused import ([e44473d](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/e44473d8d8c6593f4a31733c7f6d7843e105807b))

## 1.3.0 (2022-01-16)


### Features

* **devops:** build docker image after merge to master ([c1815e4](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/c1815e43816a3f5cfef4330fc261343f555913c4)), closes [#5](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/5)
* **devops:** build next app on push ([12b7dbe](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/12b7dbebf75ff09fad9d6df46029cf8c7e942573)), closes [#3](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/3)
* **devops:** generate version and changelog on build ([5e2fa69](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/5e2fa6940612906662a83c3cdd3cecd4d41c57fc)), closes [#4](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/4)
* **devops:** Implement sonar cloud ([62cbd87](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/62cbd877d9ef8560afe7daa84cf0ea7f86a42c4c)), closes [#1](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/1)
* **devops:** run unit tests on build ([1929d0b](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/1929d0bd5b2e9b6d47c448b13a2e25cc085ff15c)), closes [#2](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/issues/2)
* **homepage:** write down some more information what is this project about ([76fa419](https://gitlab.com/georgialexandrov/devops-telerik-course-final-project/commit/76fa419d15b91cbe2bd98b99847010d257cff343))
